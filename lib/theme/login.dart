import 'package:flutter/material.dart';


ThemeData buildDarkTheme() {
  final ThemeData base = ThemeData();
  return base.copyWith(
    primaryColor: Colors.black,
    accentColor: Colors.blue,
    scaffoldBackgroundColor: Colors.black,
    primaryTextTheme: buildTextTheme(base.primaryTextTheme, Colors.white),
    primaryIconTheme: base.iconTheme.copyWith(color: Colors.pinkAccent),
    buttonColor: Colors.pinkAccent,
    hintColor: Colors.blueAccent,
    textTheme: buildTextTheme(base.textTheme, Colors.white),    
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(),
      labelStyle: TextStyle(
        color: Colors.blue,
        fontSize: 24.0
      ),
    ),
  );
}
TextTheme buildTextTheme(TextTheme base, Color color) {
  return base.copyWith(
    body1: base.headline.copyWith(color: color, fontSize: 16.0),
    caption: base.headline.copyWith(color: color),
    display1: base.headline.copyWith(color: color),
    button: base.headline.copyWith(color: color),
    headline: base.headline.copyWith(color: color),
    title: base.title.copyWith(color: color),
  );
}