import 'package:flutter/material.dart';



class home extends StatelessWidget{
 static const String routeName="/home";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
      ),
      drawer: Drawer(
        child:ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  margin: EdgeInsets.zero,
                  padding: EdgeInsets.zero,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              'https://i.pinimg.com/originals/35/11/9d/35119d38d4ba27a41d05be6a1c93a835.jpg'))),
                  child: Stack(children: <Widget>[

                    Positioned(
                      bottom: 0,
                      right: 13.0,
                      child: Container(
                        width: 20,
                        height: 720,
                        color: Color.fromRGBO(0, 0, 0, 0.4),
                       
                      ),
                    ),
                     Positioned(
                      bottom: 12.0,
                      left: 13.0,
                      child: Container(
                        width: 550,
                        height: 20,
                        color: Color.fromRGBO(255, 255, 255, 0.4),
                        child: Text("Rinoa Heartilly",
                            style: TextStyle(
                                
                                color: Color.fromRGBO(0, 0, 0, 1.0),
                                fontSize: 20.0,
                                fontWeight: FontWeight.w500)),
                      ),
                    ),
                  ])),


            ]

        )
      ),
      
    );
  }

}