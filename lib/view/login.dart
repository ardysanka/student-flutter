import 'package:flutter/material.dart';
import 'package:student_flutter/view/home.dart';


class Login extends StatelessWidget{
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          // color: Color.fromRGBO(33, 117, 245, 1.0)
          gradient: LinearGradient(
            //  begin: Alignment(-1.0, -4.0),end: Alignment(1.0, 4.0), bagus tapi kayak kurang
                  begin: Alignment.topLeft, end: Alignment.bottomRight, // sementara pake ini dulu
                  colors: <Color>[
                    Color.fromRGBO(41, 128, 185, 1.0),
                    Color.fromRGBO(109, 213, 250, 1.0),
                    Color.fromRGBO(255, 255, 255, 1.0),
                  ],
                ),
          // image:DecorationImage(
          //   image: NetworkImage("https://calon.budiluhur.ac.id/assets/images/background/bl_background.jpg")
          //  )
        ),
      child: Center(  
        child: Container(
          width: 290,
          height: 300,
          // decoration:BoxDecoration(
          //   color: Color.fromRGBO(255, 255, 255, 0.7)
          //   ),
          child:Center(
            child:ListView(
              children:<Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,0,0,40),
                      child: Image(
                        image: AssetImage('asset/logo_bl.png')
                        ),
                      ),
                      TextField(
                    decoration: InputDecoration(
                        // border: new OutlineInputBorder(
                        //   borderSide: new BorderSide(
                        //     color: Colors.pink
                        //     ),
                        //   // borderRadius: const BorderRadius.all(
                        //   //   const Radius.circular(30.0)
                        //   // ),
                          
                        // ),
                        // labelStyle: TextStyle(
                        //           color: Colors.yellow,
                        //           fontSize: 24.0
                        //         ),
                        hintText: "NIM",
                        icon: Icon(Icons.person, 
                        // color: Color.fromRGBO(255, 255, 0, 1.0),
                        ),
                      ),
                  ),
                      TextField(
                  decoration: InputDecoration(
                      // border: new OutlineInputBorder(
                      //   borderSide: new BorderSide(
                      //     color: Colors.pink
                      //     ),
                      //   // borderRadius: const BorderRadius.all(
                      //   //   const Radius.circular(30.0)
                      //   // ),
                        
                      // ),
                      // labelStyle: TextStyle(
                      //           color: Colors.yellow,
                      //           fontSize: 24.0
                      //         ),
                      
                      hintText: "Password",
                      icon: Icon(Icons.vpn_key, 
                      // color: Color.fromRGBO(255, 255, 0, 1.0),
                      ),
                    ),
                    obscureText:true,
                ),
                RaisedButton(
                  onPressed: (){
                    Navigator.pushReplacementNamed(context, home.routeName);
                  },
                  shape:RoundedRectangleBorder(borderRadius:BorderRadius.circular(50)),
                  textColor: Colors.white,
                  color: Color.fromRGBO(41, 128, 185, 1.0), 
                  child: const Text("Login"),)
              ]
            )
          )
        )
        )
      ),
     );
  }
}

